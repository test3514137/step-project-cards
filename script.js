class Modal {
    constructor(modalId, loginBtnId, closeBtnId) {
        this.modal = document.getElementById(modalId);
        this.loginBtn = document.getElementById(loginBtnId);
        this.closeBtn = document.getElementById(closeBtnId);

        this.loginBtn.addEventListener('click', () => {
            this.openModal();
        });

        this.closeBtn.addEventListener('click', () => {
            this.closeModal();
        });

        window.addEventListener('click', (event) => {
            if (event.target == this.modal) {
                this.closeModal();
            }
        });
    }

    openModal() {
        this.modal.style.display = 'block';
    }

    closeModal() {
        this.modal.style.display = 'none';
    }
}

class Visit {
    constructor(patientName, date, diagnosis) {
        this.patientName = patientName;
        this.date = date;
        this.diagnosis = diagnosis;
    }
    getDiagnosis() {
        return this.diagnosis
    }

}


class VisitDentist extends Visit {
    constructor(patientName, date, diagnosis, tooth, visitPurpous) {
        super(patientName, date, diagnosis);
        this.tooth = tooth;
        this.visitPurpous = visitPurpous;
    }
    getVisitPurpous() {
        return this.visitPurpous

    }
}

class VisitCardiologist extends Visit {
    constructor(patientName, date, diagnosis, heartRate, age) {
        super(patientName, date, diagnosis);
        this.heartRate = heartRate;
        this.age = age;
    }
    getHeartRate() {
        return this.heartRate

    }
}

class VisitTherapist extends Visit {
    constructor(patientName, date, diagnosis, symptoms, urgency) {
        super(patientName, date, diagnosis);
        this.symptoms = symptoms;
        this.urgency = urgency;
    }
    getUrgency() {
        return this.urgency
    }
}


// const modal = new Modal('modal', 'login-btn', 'close-btn');
// const genericVisit = new Visit(patientName, new Date(), diagnosis);
//   const dentalVisit = new VisitDentist(patientName, new Date(), diagnosis, tooth, visitPurpous);
//   const heartVisit = new VisitCardiologist(patientName,  new Date(), diagnosis, heartRate, age);
//   const therapyVisit = new VisitTherapist(patientName, new Date(), diagnosis, symptoms, urgency);



document.addEventListener('DOMContentLoaded', function () {

    if (localStorage.getItem('visitCards') === null) {

        document.getElementById('no-items-msg').style.display = 'block';
    } else {

        document.getElementById('no-items-msg').style.display = 'none';


    }

    let modalRegist = document.getElementById('modal');
    let loginBtn = document.getElementById('login-btn');
    let closeBtn = document.getElementById('close-btn');

    loginBtn.addEventListener('click', function () {
        modalRegist.style.display = 'block';
    });

    closeBtn.addEventListener('click', function () {
        modalRegist.style.display = 'none';
    });

    window.addEventListener('click', function (event) {
        if (event.target == modalRegist) {
            modalRegist.style.display = 'none';
        }
    });

});

let token = '9256b8f8-c4d6-4f1e-9c5e-ef856c176a4f';

document.getElementById('visit-submit').addEventListener('click', function () {
    let title = document.getElementById('visit-title-input').value;
    let status = document.getElementById('visit-status-select').value;
    let priority = document.getElementById('visit-priority-select').value;

    let visitData = {
        title: title,
        status: status,
        priority: priority
    };

    let token = '9256b8f8-c4d6-4f1e-9c5e-ef856c176a4f';

    fetch('https://ajax.test-danit.com/api/v2/cards', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(visitData)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log('Response from server:', data);
            let noItemsParagraph = document.getElementById('no-items-msg');
            noItemsParagraph.style.display = 'none';


            return fetch("https://ajax.test-danit.com/api/v2/cards/1", {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            });
        })
        .then(response => {
            // if (!response.ok) {
            //   throw new Error('Network response was not ok');
            // }
            console.log('DELETE request successful');

            return fetch('https://ajax.test-danit.com/api/v2/cards', {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log('GET request successful:', data);
        })
        .catch(error => {
            console.error('There was a problem with the request:', error);
        });
});






let createVisit = document.getElementById('visit-modal');
let createVisitBtn = document.getElementById('visit-submit');

createVisitBtn.addEventListener('click', function () {
    createVisit.style.display = 'block';
    createVisitBtn.style.display = 'none';
});


let closeVisit = document.getElementById('close-window');
closeVisit.addEventListener('click', function () {
    createVisit.style.display = 'none';
    createVisitBtn.style.display = 'block';
});





document.getElementById('doctorSelect').addEventListener('change', function () {
    let doctor = this.value;
    let additionalFields = document.getElementById('additionalFields');
    additionalFields.innerHTML = '';

    let commonFields = `
                <label for="visitPurpose">Мета візиту:</label>
                <input type="text" id="visitPurpose" name="visitPurpose"><br>
                <label for="visitDescription">Короткий опис візиту:</label>
                <input type="text" id="visitDescription" name="visitDescription"><br>
                <label for="urgency">Терміновість:</label>
                <select id="urgency" name="urgency">
                    <option value="normal">Звичайна</option>
                    <option value="priority">Пріоритетна</option>
                    <option value="emergency">Невідкладна</option>
                </select><br>
                <label for="name">ПІБ:</label>
                <input type="text" id="name" name="name"><br>
            `;

    if (doctor === 'cardiologist') {
        additionalFields.innerHTML = commonFields + `
                    <label for="bloodPressure">Звичайний тиск:</label>
                    <input type="text" id="bloodPressure" name="bloodPressure"><br>
                    <label for="bmi">Індекс маси тіла:</label>
                    <input type="text" id="bmi" name="bmi"><br>
                    <label for="heartDiseases">Перенесені захворювання серцево-судинної системи:</label>
                    <input type="text" id="heartDiseases" name="heartDiseases"><br>
                    <label for="age">Вік:</label>
                    <input type="text" id="age" name="age"><br>
                `;
    } else if (doctor === 'dentist') {
        additionalFields.innerHTML = commonFields + `
                    <label for="lastVisitDate">Дата останнього відвідування:</label>
                    <input type="date" id="lastVisitDate" name="lastVisitDate"><br>
                `;
    } else if (doctor === 'therapist') {
        additionalFields.innerHTML = commonFields + `
                    <label for="age">Вік:</label>
                    <input type="text" id="age" name="age"><br>
                `;
    }
});


document.getElementById('visit-btn').addEventListener('click', function () {
    let doctorSelect = document.getElementById('doctorSelect');
    let selectedDoctor = doctorSelect.options[doctorSelect.selectedIndex].text;

    let title = document.getElementById('visit-title-input').value;
    let status = document.getElementById('visit-status-select').value;
    let priority = document.getElementById('visit-priority-select').value;
    let lastVisitDate = '';
    let bloodPressure = '';
    let bmi = '';
    let heartDiseases = '';

    if (selectedDoctor === 'Стоматолог') {
        lastVisitDate = document.getElementById('lastVisitDate').value;
    } else if (selectedDoctor === 'Кардіолог') {
        bloodPressure = document.getElementById('bloodPressure').value;
        bmi = document.getElementById('bmi').value;
        heartDiseases = document.getElementById('heartDiseases').value;
    }

    let visitPurpose = document.getElementById('visitPurpose').value;
    let visitDescription = document.getElementById('visitDescription').value;
    let urgency = document.getElementById('urgency').value;
    let name = document.getElementById('name').value;

    let visitData = {
        title: title,
        status: status,
        priority: priority,
        doc: selectedDoctor,
        purpose: visitPurpose,
        description: visitDescription,
        urgency: urgency,
        name: name,
        lastVisitDate: lastVisitDate,
        bloodPressure: bloodPressure,
        bmi: bmi,
        heartDiseases: heartDiseases
    };

    fetch('https://ajax.test-danit.com/api/v2/cards', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(visitData)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log('Response from server:', data);

            createCard(data);

            closeModal();
        })
        .catch(error => {
            console.error('There was a problem with the POST request:', error);
        });
});



function deleteCard(cardId) {
    fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`
        },
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            console.log('DELETE request successful');
        })
        .catch(error => {
            console.error('There was a problem with the DELETE request:', error);
        });
}


function createCard(cardData) {
    let cardElement = document.createElement('div');
    cardElement.classList.add('card');
    cardElement.innerHTML = `
      <span id="close-btn" class="close">&times;</span>
      <h3>${cardData.title}</h3>
      <p>Лікар: ${cardData.doc}</p>
      <p>Мета: ${cardData.purpose}</p>
      <p>Опис: ${cardData.description}</p>
      <button id="show-more">Показати більше</button>
      <div class="additional-info" style="display: none;">
          ${cardData.age ? `<p>Вік: ${cardData.age}</p>` : ''}
          ${cardData.lastVisitDate ? `<p>Дата останнього візиту: ${cardData.lastVisitDate}</p>` : ''}
          ${cardData.bloodPressure ? `<p>Тиск: ${cardData.bloodPressure}</p>` : ''}
          ${cardData.bmi ? `<p>Індекс маси тіла: ${cardData.bmi}</p>` : ''}
          ${cardData.heartDiseases ? `<p>Перенесені захворювання серцево-судинної системи: ${cardData.heartDiseases}</p>` : ''}
          <p>Терміновість: ${cardData.urgency}</p>
          <p>Ім'я: ${cardData.name}</p>
          <p>Статус: ${cardData.status}</p>
          <p>Пріоритетність: ${cardData.priority}</p>
      </div>
      
  `;

    document.getElementById('visit-cards').appendChild(cardElement);

    let closeCard = cardElement.querySelector('.close');
    closeCard.addEventListener('click', function () {
        deleteCard(cardData.id);
        cardElement.remove();
    });

    let showMoreBtn = cardElement.querySelector('#show-more');
    showMoreBtn.addEventListener('click', function () {
        let additionalInfo = cardElement.querySelector('.additional-info');
        let buttonText = showMoreBtn.innerText;
        if (buttonText === "Показати більше") {
            additionalInfo.style.display = 'block';
            showMoreBtn.innerText = "Приховати";
        } else {
            additionalInfo.style.display = 'none';
            showMoreBtn.innerText = "Показати більше";
        }
    });
}
function closeModal() {
    let modal = document.getElementById('modal');
    if (modal) {
        modal.style.display = 'none';
    } else {
        console.error('Modal element not found');
    }
}
